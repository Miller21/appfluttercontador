

import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     
     return Scaffold(
      appBar: AppBar(title: const Text("Pantalla 2"),),
      body: Center(child: Column(
          children: const [
              FlutterLogo(size: 200,),

              SizedBox(height: 10 ),

              SizedBox(
              height: 200,
              width: double.infinity ,
               child: Card(
                color: Colors.red,
                child: Text("Mi primera tarjeta"),
               )),
               
                SizedBox(
                height: 200,
               child: Image(image: NetworkImage("https://www.purina-latam.com/sites/g/files/auxxlc391/files/styles/social_share_large/public/purina-por-que-vemos-tan-felices-a-los-gatos-en-cajas-de-carton.png?itok=xABhZ8v9"),),
               )
          ],
      ),)

     );

    
  }
}